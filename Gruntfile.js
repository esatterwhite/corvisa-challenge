var fs   = require('fs')
  , path = require('path')
  ,PROJECT_DIR = path.resolve( '.' )


module.exports = function( grunt ){

	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json')
		,clean:["./doc", "./js/build", ".sass-cache"]
		,compass:{
			production:{
				options:{
					config:"./theme/config.rb"
					,sassDir:"./theme/scss"
					,cssDir:"./css"
					,environment:'production'
				}
			}
		}
		,jsdoc:{
			"core":{
				src:['js/modules/**/*.js']
				,jsdoc:path.normalize( path.join( PROJECT_DIR,"bin" ) )
				,destination:"build/docs"
			}
		}
		,replace:{
			main:{
				options:{
						patterns:[
							{
								match:'timestamp'
								,replacement:(+ new Date() )
							}
						]
				}
				,files:[
					{
						src:['js/lib/require-init.js']
						,expand: false
						,dest:'js/build/require-init.js'
					}
				]
			}
		}
		,requirejs:{
			compile:{
				options:{
					appDir:"js/modules"
					,dir:"js/build"
					,baseUrl:"."
					,modules:[
						{
							name:"conf"
							,exclude:["mootools","mooml"]
						}
						,{
							name:"mvc/view"
							,exclude:["mootools","mooml"]
						}
						,{
							name:'mvc/router'
							,exclude:["mootools","mooml"]
						}
						,{
							name:"mvc/controller"
							,exclude:["mootools","mooml"]
						}
						,{
							name:'forecast'
							,exclude:["mootools","mooml"]
						}
						,{
							name:"accessor"
							,exclude:["mootools","mooml"]
						}
						,{
							name:"localstorage"
						}
						,{
							name:"controllers"
							,exclude:["mootools","mooml"]
						}
						,{
							name:"urls"
							,exclude:["mootools","mooml"]
						}
						,{
							name:"core/views"
							,exclude:["mootools","mooml"]
						}
					]
					,paths:{
						"mootools":"../lib/mootools"
						,"mooml":"../lib/mooml"
					}
				}
			}
		}
		,jshint:{
			all:['js/modules/**/*.js']
			,jshintrc:"./.jshintrc"
			,ignore_warning:{
				options:{
					"-W030":true
					,"-W098":true
				}
			}
			,options:{
				"curly":true,
				"smarttabs":true,
				"eqnull":true,
				"latedef":true,
				"undef":true,
				"unused":true,
				"laxcomma":true,
				"laxbreak":true,
				"evil":true,
				"globals":{
					"require":true,
					"exports":true,
					"viewport":true,
					"module":true,
					"define":true,
					"window":true,
					"getScrollSize":true,
					"Class":true,
					"Mooml":true,
					"router":true,
					"history":true,
					"setInterval":true,
					"clearInterval":true,
					"setTimeout":true,
					"clearTimeout":true,
					"document":true,
					"postMessage":true,
					"Elements":true,
					"Element":true,
					"$":true,
					"Events":true,
					"Options":true,
					"typeOf":true,
					"Image":true,
					"alert":true,
					"eval":true,
					"navigator":true,
					"location":true,
					"io":true,
					"console":true,
					"ActiveXObject":true,
					"__flash__argumentsToXML":true,
					"XMLHttpRequest":true,
					"GLOBAL":true
				}

				,"ignores":[
					"js/modules/text.js"
					, "js/modules/signals.js"
					, "js/modules/hasher.js"
					, "js/modules/functools.js"
					, "js/modules/localstorage.js"
					, "js/modules/underscore.js"
					, "js/modules/jsonselect.js"
					, "js/modules/crossroads.js"
					, "js/modules/modernizr.js"
					, 'js/modules/test/**/*.js'
				]
			}
		}
	});

	grunt.loadNpmTasks("grunt-contrib-requirejs");
	grunt.loadNpmTasks("grunt-contrib-clean");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-compass");
	grunt.loadNpmTasks("grunt-jsdoc");
	grunt.loadNpmTasks("grunt-replace");
	grunt.registerTask('default',['clean', "jshint", "compass", "jsdoc", "requirejs", "replace"])
};
