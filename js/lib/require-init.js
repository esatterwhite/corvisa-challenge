var challenge = require({
	baseUrl:"js/build"
	,urlArgs:"@@timestamp"
	,paths:{
		"mootools":"../lib/mootools"
		,"mooml":"../lib/mooml"
	}
	,shim:{
		"mootools":{
			exports:"mootools"
		}
		,"mooml":{
			exports:"Mooml"
			,deps:["mootools"]
		}
	}
})
