/*jshint laxcomma:true, smarttabs: true, mootools: true */
/**
 * Primary core module.
 * @module NAME
 * @author Eric Satterwhite
 * @requires module:core/views
 **/
define([
      "require"
    , "module"
    , "exports"
    , "core/views"
],function(require, module, exports, views){

    return {
        views:views
    };
});