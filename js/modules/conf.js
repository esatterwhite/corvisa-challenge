/*jshint laxcomma:true, smarttabs: true, mootools:true */
/**
 * Holds application level settings and configurations
 * @module conf
 * @author Eric Satterwhite
 * @requires localstorage
 **/
 define([
	'require'
	,'module'
	,'exports'
	,'localstorage'
 ], function(require, module, exports, localstorage ){

	var settings = {
	};
	var default_units = [ "imperial", "metric" ];
	var defaults={
		location:"Milwaukee,WI"
		,range:3
		,longitude:null
		,latitude:null
		,units:"imperial"

	};

	Object.defineProperties(settings,{
		/**
		 * DESCRIPTION
		 * @property module:settings.location
		 **/
		location:{
			enumerable:true
			,get: function(){
				return localstorage.get("location") || defaults.location;
			}

			,set:function(value){
				localstorage.set('location', value);
				return this;
			}
		}
		/**
		 * DESCRIPTION
		 * @property module:settings.longitude
		 **/
		,longitude:{
			enumerable:true
			,get: function(){
				return localstorage.get('longitude');
			}

			,set:function(value){
				localstorage.set('longitude', value);
				return this;
			}

		}
		/**
		 * DESCRIPTION
		 * @property module:settings.latitude
		 **/
		,latitude: {
			enumerable:true
			,get: function(){
				return localstorage.get('latitude') || null;
			}
			,set:function( value ){
				localstorage.set('latitude', value );
				return this;
			}

		}
		/**
		 * DESCRIPTION
		 * @property module:settings.range
		 **/
		,range: {
			enumerable:true
			,get: function(){
				return localstorage.get('range') || defaults.range;
			}
			,set: function(value){
				localstorage.set('range', value);
				return this;
			}
		}
		/**
		 * The
		 * @property module:settings.units
		 **/
		,units: {
			enumerable:true
			,get: function(){
				return localstorage.get('units') || defaults.units;
			}
			,set: function(value){
				value = value.toLowerCase();
				if( value in default_units ){
					localstorage.set('units', value);
				}
				return this;
			}
		}
		,save: {
			enumerable:false
			,value: function( key_or_object, value ){
				if( typeOf( key_or_object ) == "object"){
					for(var key in key_or_object ){
						localstorage.set( key, key_or_object[ key ]);
					}
				} else{
					localstorage.set(key_or_object, value );
				}

			}
		}

		,reset: function(){
			localstorage.clear();
		}

	});

	return settings;
});
