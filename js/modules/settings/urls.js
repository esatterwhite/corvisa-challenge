/*jshint laxcomma:true, smarttabs: true */
/**
 * settings url module
 * @module module:settings/urls
 * @author Eric satterwhite
 **/
define({
	"settings/configure":{"controller":"settings.controller", action:"configure"}
	,"settings/load:?query:":{controller:"settings.controller", action:"load"}
});