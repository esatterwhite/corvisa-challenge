/*jshint laxcomma:true, smarttabs: true */

/**
 * Form for loading location settings
 * @module module:settings/views/forms/location
 * @author Eric Satterwhite
 * @requires module:mvc/view
 * @requires mootools
 * @requires mooml
 **/
define([
	'require'
	,'module'
	,'exports'
	,'mvc/view'
	,'mootools'
	,'mooml'
],function(require, module, exports, View){
	var LocationForm;
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	LocationForm = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends: View
		,options:{
			tpl: function( data ){
				var tags = Mooml.engine.tags;
				var that = this;
				tags.form({'id':'settingsform'}
						,tags.div(
								tags.input({'type':'hidden', 'id':'long', 'name':'longitude', 'placeholder':'longitude', value:data.longitude})
								,tags.input({'type':'hidden', 'id':'latitude', name:'latitude', 'placeholder':'latitude', value:data.latitude})
								,tags.input({'type':'text', 'id':'long', name:'location', 'placeholder':'Location', value:data.location})
								,tags.div({'class':'large-9 small-9 medium-9 columns', styles:{padding:0}}
									,tags.input({
										'type':'number'
										, 'id':'long'
										, 'name':'range'
										, 'placeholder':'Forecast'
										, 'min':3
										, 'max':5
										, 'value':data.range
									})
								)

								,tags.div({'class':'small-3 columns radius', styles:{padding:0}}
									,tags.span({'class':'postfix radius'}, 'days')
								)
								,tags.select({'id':'units', 'name':'units', value:data.units}
									,tags.option({value:'imperial', selected: data.units == 'imperial'},'Imperial')
									,tags.option({value:'metric', selected:data.units == 'metric'},'Metric')
								)
						)

					,tags.div({'class':'columns', styles:{padding:0}}
						,tags.a({ href:'#', 'class':'button tiny right radius large-3 small-12'
							,events:{
								'click':function( evt ){
									evt.stop();
									var data = evt.target.getParent('form').toQueryString().parseQueryString();
									that.fireEvent('submit', [data] );
									that.fireEvent('press', [data] );
								}
							}
						}, 'submit')
						,tags.a({href:'#', 'class':'button tiny alert left radius large-3 small-12'
								,events:{
									'click':function( evt ){
										evt.stop();
										that.fireEvent('cancel');
										that.fireEvent('press',[null]);
									}
								}
							}
							, 'cancel'
						)

					)
				);
			}
		}
		,initialize: function( options ){
			this.parent( options );
		}
	});
	return LocationForm;
});
