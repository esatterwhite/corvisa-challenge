/*jshint laxcomma:true, smarttabs: true */

/**
 * forms harness for settings view sub application
 * @author Eric satterwhite 
 * @requires module:settings/views/forms/location
 **/
define([
	'require'
	,'module'
	,'exports'
	,'settings/views/forms/location'
],function(require, module, exports, location){
	return{
		LocationForm: location
	};
});