/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * primary views harness for settings application
 * @module module:settings/views
 * @author Eric Satterwhite
 * @requires module:settings/views/forms
 * @requires moduleB
 * @requires moduleC
 **/
define([
	'require'
	,'module'
	,'exports'
	,'settings/views/forms'
],function(require, module, exports, forms){
	return{
		forms:forms
	};
});