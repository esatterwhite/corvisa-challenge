/*jshint laxcomma:true, smarttabs: true */
/**
 * Primary controller harness for settings application
 * @module module:settings/controller
 * @author Eric Satterwhite
 * @requires module:settings/controllers/configuration
 **/
define([
	'require'
	,'module'
	,'exports'
	,'settings/controllers/configuration'
],function(require, module, exports, configuration ){
		return{
			"settings.controller":configuration
		};
	}
);
