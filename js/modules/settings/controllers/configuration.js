/*jshint laxcomma:true, smarttabs: true, -W030: true */
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric satterwhite
 * @requires module:core/views
 * @requires module:conf
 * @requires module:settings/views/forms/location
 **/
define([
	'require'
	,'module'
	,'exports'
	,'core/views'
	,'settings/views/forms/location'
	,'conf'
],function(require, module, exports, views, LocationForm, conf){

    /**
	* DESCRIPTION
	* @class module:NAME.Thing
	* @param {TYPE} NAME DESCRIPTION
	* @example var x = new NAME.Thing({});
	*/
	return {
		load: function( /* options */ ){

		}
		,reset: function( ){
			conf.reset();
		}
		,configure: function( /* options */ ){
			var m = new views.Modal({
				title:'Settings'
				,closeable:false
				,onBoxclose: function( ){
					router.dispatch('ui/nav/reset');
				}
				,stage:new LocationForm({
					data:conf
					,onSubmit: function( values ){
						conf.save( values );
					}
					,onPress:function(){
						m.hide();
						m.hideBox(true);
						history.length ? history.back():router.redirect("challenge");
					}
				})
			});
			m.show();
		}
	};
});
