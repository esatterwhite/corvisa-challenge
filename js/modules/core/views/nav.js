/*jshint laxcomma:true, smarttabs: true, mootools: true */
/**
 * One off view instance to render navigation
 * @module NAME
 * @author Eric Satterwhite
 * @requires module:mvc/view
 **/
define([
	'require'
	,'module'
	,'exports'
	,'mvc/view'
	,'mooml'
], function( require, module, exports, View /* Moomle */ ){
		var Nav;

		Nav = new View({
			name:"corvisanav"
			,tpl:function(){
				var tag = Mooml.engine.tags;

				var that = this;
				tag.nav({'class':'top-bar' , 'styles':{overflow:'inherit'}},
					tag.ul({'class':'title-area hide-for-small'},
						tag.li({'class':'name'},
							tag.h1(
								tag.a('Weather And Junk')
							)
						)
					),
					tag.section(
						{
							'class':'top-bar-section'
							,'events':{
								'click:relay(a)':function(evt){
									$$('nav li').removeClass('active');
									evt.target.getParent('li').addClass('active');

								}.bind( that )
							}
						},
						tag.ul({'class':'right'},
							tag.li(
								tag.a({'href':'#challenge/forecast/current','text': 'Current Conditions'})
							)
							,tag.li(
								tag.a({'href':'#challenge/forecast/extended', 'text':'Extended Forecast'}) // end a
							)// end li
							,tag.li(
								tag.a({'href':'#challenge/settings/configure', 'text':'Settings'}) // end a
							) // end li
							,tag.li(
								tag.a({'href':'https://bitbucket.org/esatterwhite/corvisa-challenge/src', 'text':'Source'}) // end a
							) // end li
						) // end ul
					) // end section
				);
			}
		});
		return Nav;
	}
);
