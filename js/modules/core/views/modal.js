/* globals getScrollSize */
/*jshint laxcomma:true, smarttabs: true, mootools: true,  -W098:true */
/**
 * Contains view for rendering modal windows
 * @module module:core/views/modal
 * @author Eric Satterwhite
 * @requires ui/overlay
 **/
define([
		'require'
		,'module'
		,'exports'
		,'core/views/overlay'
	],function(require, module, exports, Overlay ){
	var ModalBox;
	/**
	 * Modal Window class
	 * @class module:core/views/modal.Modal
	 * @extends module:core/view/overlay.Overlay
	 * @mixes Options
	 * @mixes Events
	 * @param {Object} options Config options for a modal instance
	 * @param {String} [options.title="Modal box"] Title to display on the header of the window
	 * @param {Element|Template} options.stage an html fragment or template instance to render in the window.
	 * @param {String} [options.className="panel smallbox"] css class to apply to the window when rendered
	 * @example var x = new NAME.Thing({});
	 */
	ModalBox = new Class(/** @lends module:core/views/modal.Modal.prototype */{
		Extends:Overlay,
		Implements:[Events, Options],

		options:{
		  title:'Modal Box',
		  stage:null,
		  className: 'panel smallbox',
		  onBoxopen: Function.from(),
		  onBoxclose: Function.from()
		},

		initialize:function(options){
			this.parent(options);
			this.setOptions(options);
		},
		TrackInstances:true,

		/**
		 * build the modal view
		 * @private
		 * @method module:core/views/modal.Modal#build
		 **/
		build:function(){
		  var m, d, dim, box, titleBar, closebutton, stagewrap, stagecontainer, stage;
		  m   = this.options._mask;
		  dim = getScrollSize();

		  //check for fuggin IE people...
		  this.yScroll = window.pageYOffset ? window.pageYOffset : document.documentElement.scrollTop;

		  //primary container
		  box = new Element('div', {
			  'class': 'modalbase '  + this.options.className,
			  styles: {
				  position: 'absolute',
				  'z-index': this.baseIndex,
				  opacity: 0
			  },
			  events: {
				  dblclick: function( ){
					  if (this.options.closeable) {
						  this.hide();
						  this.hideBox(true);
					  }
				  }.bind(this)
			  }

		  });
		  d = new Element('div', {
			'class':'bg-dark iwrap'
		  }).inject(box,'top');

		  titleBar = new Element('h2', {
			  'class': 'title draggable',
			  html: '<small>{title}</small>'.substitute({title:this.options.title})
		  }).inject(d,'top');

		  stagewrap = new Element('div', {
			  id: 'stagewrap',
			  'class': 'bg-deep p_all-8'
		  }).inject(box);

		  stagecontainer = new Element('div', {
			  id: 'stagecontainer'
			  ,'class':'row'
		  }).inject(stagewrap);

		  stage = new Element('div', {
			  id: 'stage',
			  'class': 'border-light p_all-4'
		  }).inject(stagecontainer);

		  //inject everything into the 'box'

		  // set the stage contents and save to object
		  stage.adopt( $(this.options.stage) );

		  this.setOptions({
			  stage: stage,
			  titleBar: titleBar,
			  modalBox: box,
			  closebutton: closebutton
		  });
		  box.inject(this.element,'after');
		  this.reposition();
		  new Drag.Move(box, {
			  handle: titleBar,
			  container:this.element
		  });
		}.protect(),
		/**
		 * @private
		 * @method reposition sssmoved the box in relation to it's parent container
		 * its position is a function of the number of instances created
		 */
		reposition:function(){
			var box = this.options.modalBox,
				el  = this.options.renderTo;

			this.options.modalBox.position({
				relativeTo: this.options.renderTo,
				position: 'topLeft',
				offset:{
					x:(el.getSize().x / 2) - (box.getSize().x / 1.5) + (15 * ModalBox.instances.length),
					y:( (30 * ModalBox.instances.length ))}
			});
		}.protect(),

		/**
		 * builds the dom structre and displays the window
		 * @chainable
		 * @method module:core/views/modal.Modal#function
		 * @param {Object} [options] Any config params the constructor accepts to be applied before display 
		 * @return Modal current class instance
		 **/
		show: function(options){
			this.setOptions( options );
			this.parent();
			this.build();
			this.options.modalBox.fade( 'in' );

			/**
			 * @name moduleName.Thing#shake
			 * @event
			 * @param {Modal} current instance
			 */
			this.fireEvent('boxopen', this);

			return this;
		},

		/**
		 * hides the instance from view
		 * @chainable
		 * @param {Boolean} [erase=false] if set to true the window will be destroyed and removed from the dom
		 * @method module:core/views/modal.Modal#hidebox
		 **/
		hideBox: function(erase){
			var morph = new Fx.Morph(this.options.modalBox);
			morph.addEvent('complete', function( ){
				/**
				 * @name moduleName.Thing#shake
				 * @event
				 * @param {Event} e
				 * @param {Boolean} [e.withIce=false]
				 */
				this.fireEvent('boxclose', this);
				if( erase ){
					this.destroy();
				}
			}.bind( this ));

			this.hide();
			morph.start({
				opacity:0
			});
			return this;
		},

		/**
		 * Destroys the current instance and removes it from the window manager
		 * @private
		 * @method module:core/views/modal.Modal#destroy
		 **/
		destroy:function(){
			this.parent();
			if( this.options.modalBox ){
				this.options.modalBox.destroy();
				this.options.modalBox = null;
			}
			ModalBox.instances.erase(this);

		},

		/**
		 * returns the inner dom element
		 * @method module:core/views/modal.Modal#toElement
		 * @return Element
		 **/
		toElement:function(){
			return this.options.stage || null;
		}
	});

	// Factory Style Function
	// Closes all instances of ModalBox
	ModalBox.closeAll = function(){
		Array.each(ModalBox.instances, function(item,index, obj){
			item.hideBox();
			item.destroy.delay(700, item);
			item = null;
		});
		ModalBox.instances.empty();
	};

	return ModalBox;
});
