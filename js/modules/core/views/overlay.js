/*jshint laxcomma:true, smarttabs: true, mootools: true */
/**
 * Provides overlay mask for specific dome elements
 * @module module:core/views/overlay
 * @author Eric Saatterwhite
 * @requires module:mvc/view
 * @requires mootools
 * @requires mooml
 **/
define([
        'require'
        ,'module'
        ,'exports'
        ,'mvc/view'
        ,'mootools'
        ,'mooml'
    ],function(require, module, exports, View /* Mootools, Mooml */){

        Class.Mutators.TrackInstances = function(track){
            if(!track) {return;}

            var oldInit = this.prototype.initialize;
            var klass = this;

            klass.prototype.initialize = function(){
                (klass.instances = klass.instances || []).push(this);
                oldInit.apply( this, arguments);
            };
        };


        /**
         * DESCRIPTION
         * @class module:NAME.Thing
         * @param {TYPE} NAME DESCRIPTION
         * @example var x = new NAME.Thing({});
         */
        var Overlay = new Class(/** @lends module:NAME.Thing.prototype */{
            Extends:View
            ,Implements:[Events, Options]
            /**
             *
             * @param {String} bgColor hex code of the color of the overlay
             * @param {Number} opacity the opacity to render the overlay at
             * @param {Boolean} closeable
             * @param {element} renderTo element or element id to render tempalte output
             */
            ,options:{
                bgColor:'#000000'
                ,opacity:0.6
                ,closeable:false
                ,override:false
                ,autodestroy:false
                ,renderTo:document.body
                ,tpl: function( ){
                    var tag = Mooml.engine.tags;
                       tag.div({
                        id:String.uniqueID(),
                        styles: {
                            position: 'fixed',
                            height: '100%',
                            width: '100%',
                            'background-color': this.options.bgColor,
                            opacity: 0,
                            top:0,
                            left:0,
                            visibility:'hidden',
                            'z-index': this.baseIndex

                        },
                        events: {
                            dblclick: function( ){
                                if(this.options.closeable){
                                    this.hide();
                                }
                            }.bind(this)
                        }
                    });
                }
            }
            ,element:null
            ,isMaskOn: false
            ,initialize:function(options){
                // set base options
                this.parent(options);
                this.baseIndex = 9999 + Overlay.instances.length;
                // we use the number of instances to increase the z-index
                // as new instances are created
                this.render();
                this.inject();
                if( this.options.renderTo != document.body ){
                    this.options.renderTo = document.id( this.options.renderTo );
                    this.position();
                }
            }
            ,TrackInstances:true

            /**
             * Positions the overlay appropriatly in the document
             * @private
             * @method module:overlay.Overlay#position
             **/
            ,position:function(){
                // this.resize();
                this.element.position({
                    relativeTo:this.options.renderTo,
                    position:'topLeft'
                });
            }
            /**
             * Injects the overlay into the dom structure
             * @private
             * @method module:overlay.Overlay#inject
             **/
            ,inject:function(){
                this.element.inject(this.options.renderTo,'top');
            }

            /**
             * Renders and fades the overlay into place
             * @method module:overlay.Overlay#show
             * @param {Object} options Any configuration options the constructor takes
             **/
            ,show:function( options ){
              if(this.isMaskOn){
                return this;
              }
                if( options ){
                    this.setOptions( options );
                }

                this.isMaskOn = true;
                this.element.fade(this.options.opacity);
                return this;
            }
            /**
             * Resizes the overlay to fit inside of the parent element
             * @method module:overlay.Overlay#resize
             * @param {Number} height
             * @param {Number} width
             **/
            ,resize:function(x, y){
                var dim = this.options.renderTo.getComputedSize({
                    styles:['padding', 'border']
                });

                this.element.setStyles({
                    width:Array.pick([x, dim.totalWidth, dim.x]),
                    height:Array.pick([y, dim.totalHeight, dim.y])
                });
            }
            /**
             * Hides the current overlay instance from view and removes from the dom
             * @method module:overlay.Overlay#hide
             **/
            ,hide:function(){
                var morph = new Fx.Morph(this.element);
                morph.addEvent('complete', function(  ){
                    /**
                     * @name moduleName.Thing#shake
                     * @event
                     * @param {Event} e
                     * @param {Boolean} [e.withIce=false]
                     */
                    this.fireEvent('maskhide');
                    if( this.options.autodestroy ){
                        this.destroy();
                    }
                }.bind( this ));
                morph.start({
                    opacity:0
                });
                this.isMaskOn = false;
                return this;
            }
            /**
             * destroys the dom and removes the instance from the overlay manager
             * @method module:overlay.Overlay#destroy
             **/
            ,destroy:function(){
                try {
                    Overlay.instances.erase(this);
                }catch(e){}
                this.element = this.element ? this.element.destroy() : null;
            }
            /**
             * returns the overlay instance element
             * @private
             * @method module:overlay.Overlay#toElement
             **/
            ,toElement:function(){
                return this.element;
            }

        });

        return Overlay;
    }
);
