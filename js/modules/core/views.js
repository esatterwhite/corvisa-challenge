/*jshint laxcomma:true, smarttabs: true */
/**
 * Provides all views required for the core module
 * @module module:core/views
 * @author Eric Satterwhite 
 * @requires module:core/views/modal
 * @requires module:core/views/nav
 * @requires module:core/views/overlay
 * @requires module:core/views/unorderedlist
 * @requires module:core/views/viewport
 **/
define([
		  "require"
		, "module"
		, "exports"
		, "core/views/modal"
		, "core/views/nav"
		, "core/views/overlay"
		, "core/views/unordedlist"
		, "core/views/viewport"
	]
	,function(require, module, exports, Modal,  Nav, Overlay, Unorderedlist, Viewport ){

		return{
			Nav:Nav
			,Overlay:Overlay
			,Unorderedlist: Unorderedlist
			,Viewport:Viewport
			,Modal:Modal
		};
	}
);
