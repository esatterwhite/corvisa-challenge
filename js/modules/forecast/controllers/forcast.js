/*jshint laxcomma:true, smarttabs: true, mootools: true */
/**
 * DESCRIPTION
 * @module NAME
 * @author module:forecast/controllers/forecast
 * @requires conf
 * @requires module:core/views/unorderedlist
 * @requires module:forecast/views/temperature
 **/
define(
    ['require', 'module', 'exports', 'core/views', 'forecast/views/temperature', 'conf']
    , function(require, module, exports, views, temp, conf ){
        var weather_request = new Request.JSONP({
            url:"http://api.openweathermap.org/data/2.5/weather"
            ,data:{}
            ,onComplete: function( result ){
                if( result.cod !== 200 ){
                    throw new Error("un able to retrieve results");
                }
                var l = new views.Unorderedlist();
                l.render({max:1});
                result.temperature = result.main.temp;
                result.units = conf.units;
                l.add( new temp({data:result} ));
                viewport.removeAll().add( l );
            }
            ,onFailure: function( ){
                throw new Error("current xhr failure");
            }
        });
    /**
     * DESCRIPTION
     * @class module:NAME.Thing
     * @param {TYPE} NAME DESCRIPTION
     * @example var x = new NAME.Thing({});
     */
    return {
		current: function(  ){

            navigator.geolocation.getCurrentPosition(function( position ){
                conf.latitude = position.coords.latitude;
                conf.longitude = position.coords.longitude;

                weather_request.setOptions({
                    data:{
                        "units":conf.units
                        ,"lat":conf.latitude
                        ,"lon":conf.longitude
                    }
                }).send();
            },function(){
                weather_request.setOptions({
                    data:{
                        "units":conf.units
                        ,q:conf.location
                    }
                }).send();
            });
		}

        ,daily: function( days, opts ){
            var options = {
                "units":conf.units
                ,"cnt":days || conf.range
            };

            options = Object.merge( options, opts || {} );

            if(!options.q && !options.lat ){
                options.q = conf.location;
            }

            new Request.JSONP({
                url:"http://api.openweathermap.org/data/2.5/forecast/daily"
                ,data:options
                ,onComplete: function( result ){
                    var l = new views.Unorderedlist()
                       , data;
                    l.render({max:options.cnt});
                    data = result.list.map( function( r ){
                        return {
                            temperature:r.temp.day
                            ,weather: r.weather[0]
                            ,temp:r.temp
                            ,dt:r.dt
                            ,name:result.city.name
                            ,units:conf.units
                        };
                    });
                    l.add( new temp({data:data} ));

                    viewport.removeAll().add( l );
                }
            }).send();
        }
	};
});
