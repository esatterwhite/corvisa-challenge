/*jshint laxcomma:true, smarttabs: true, mootools:true  */
/**
 * DESCRIPTION
 * @module module:forecast/views/temperature
 * @author Eric Satterwhite
 * @requires module:mvc/view
 * @requires mooml
 **/
define([
	"require"
	,"module"
	,"exports"
	,"mvc/view"
	,"mooml"
],function(require, module, exports, View /* Moomle */){
	var Temperature;
	function temperature_tpl( data ){
		var tag = Mooml.engine.tags;
		var weather = Array.from( data.weather)[0];
		var today = new Date( data.dt * 1000 );
		var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
		tag.li({"class":"text-center"}
			,tag.h3(
				tag.small(
					days[ today.getDay() ]
				)
			)
			,tag.h1(
				tag.div(
					{
						"data-temperature": data.temperature


					}
					,tag.span("0")
					,tag.span("&deg;")
					,tag.sup(
						tag.small(data.units == "imperial" ? "F" : "C")
					)
				)
			)
			,tag.div({"class": "tempurature " + weather.main.toLowerCase()})
			,tag.div( data.name )
			,tag.div( weather.description )
		);
	}

	Mooml.register("temperature", temperature_tpl);
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	Temperature = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:View
		,override:true
		,options:{
			tpl: temperature_tpl
			,renderTo:"viewport"
			,name:"temperature-module"
		}
		,initialize: function( options ){
			this.addEvent("afterrender", this.onAfterRender.bind( this ) );
			this.parent( options );
		}
		,onAfterRender: function( instance, data, el ){
			$$(el).each(function( e ){
				var f = new Class({
					Extends:Fx
					,options:{
						 transition:"circ:out"
						,duration:2000
					}
					,set: function(value){
						e.getFirst("h1 > div > span").set("text", Math.round(value) );
					}
				});
				new f().start(0, e.getFirst("h1 > div").get("data-temperature"));
			});
		}
	});

	return Temperature;
});
