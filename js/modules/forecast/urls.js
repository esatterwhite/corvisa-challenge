/*jshint laxcomma:true, smarttabs: true */
/**
 * urls for foreast application
 * @module module:forecast/urls
 * @author Eric satterwhite
 **/
define({
    "forecast/current:?query:":{controller:'forecast.controller', action:'current'}
    ,"forecast/extended/:days::?query:":{controller:'forecast.controller', action:'daily'}
});
