/*jshint laxcomma:true, smarttabs: true */
/**
 * primart views harness for forecast application
 * @module module:forecast/views
 * @author Eric satterwhite
 * @requires module:forecast/views/temperature
 **/
define([
    'require'
    ,'module'
    ,'exports'
    ,'forecast/views/temperature'
],function(require, module, exports, temp){
    return {
        Termperature:temp
    };
});
