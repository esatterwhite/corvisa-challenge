/*jshint laxcomma:true, smarttabs: true */
/**
 * primary controller for the forecast application
 * @module module:forecast/controllers
 * @author Eric Satterwhite
 * @requires module:forecast/controllers/forecast
 **/
define([
        'require'
        ,'module'
        ,'exports'
        ,'forecast/controllers/forcast'
    ],function(require, module, exports, forcastcontroller ){
        return{
            "forecast.controller":forcastcontroller
        };
    }
);
