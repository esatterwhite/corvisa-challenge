/* globals router, define */
/*jshint laxcomma:true, smarttabs: true, -W098:true, mootools:true */
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 **/
define(["require", "module", "exports" ],function(require, module, exports){
	return {
		"weatherapp.controller":{
			index:function(jsmodule, next){

				require([jsmodule], function(){
					router.dispatch( jsmodule + "/" + next );
				});

			}
			,resetnav: function(){
				$$("nav li").removeClass("active");
				$$("nav a[href=" + window.location.hash + "]" ).getParent("li").addClass("active");
			}
		}
	};

});
