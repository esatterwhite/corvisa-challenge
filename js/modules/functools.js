/*jshint laxcomma:true, smarttabs: true */

if( typeof define !== 'function'){
	var define = require('amdefine')( module, require );
}

/**
 * AMD Module for workind with arrays.
 * @author Eric Satterwhite
 * @module functools
 * @requires array
 */
define(
	['require','exports','module' ]
	,function(require, exports, module){
		var _wrapDumbFunction
			,enumerables
			,i
			,_extend;

		enumerables = true;
		for (i in {toString: 1}){
			enumerables = null;
		}
		enumerables = ['hasOwnProperty', 'valueOf', 'isPrototypeOf', 'propertyIsEnumerable', 'toLocaleString', 'toString', 'constructor'];

		_wrapDumbFunction = function( func ){
			var args, i;
			// covers common cases so we don't
			// have to loop every time
			switch( arguments.length){
				case 0:
					return func();
				case 1:
					return func( arguments[0]);
				case 2:
					return func( arguments[0], arguments[1], arguments[2]);
			}

			args = [];
			for( i=0; i < arguments.length; i++ ){
				args.push( 'arguments[' + i +']');
			}

			return eval('(func(' +args.join(',') + '))');
		};

		_extend = function( fn, key, value){
			if( typeof key === 'string' ){

				fn[key] = value;
			}
		};

			/**
			 * An empty function
			 **/
			exports.noop =  function(){ };

			/**
			 * Returns a function that calls the passed in function under the given context
			 * @param {Function} func the function to rebind
			 * @param {Object} self the object to bind to
			 * @return {Function}  The resulting function
			 */
			exports.bind =  function(func, self ){
				var args
					,newfn
					,F;

				if( typeof func === 'string'){
					func = self[func];
				}
				args = arguments.length > 1 ? Array.prototype.slice.call(arguments, 2) : null
				,F = function(){};

				if( typeof func === 'function' && typeof func.apply == null){
					func = _wrapDumbFunction( func );
				}

				newfn = function(){
						var length = arguments.length
						,context = self
						,result;

					if( func instanceof newfn ){
						F.prototype = self.prototype;
						context = new F;
					}

					result = ( !args && !length ) ?
									func.call( context ) : // if there are no arguments, jut call it
									func.apply( context , args && length ? args.concat( Array.prototype.slice.apply(arguments)) : args || arguments );
					return context == self ? result : context;
				};


				return newfn;

			};

			exports.run =  function( fn, scope, args, appendArgs ){
				if( arguments.length === 2 ){
					return fn.apply( scope, arguments );
				}

				var method = fn
					,slice = Array.prototype.slice;

					return function(){
						var callArgs = args || arguments;

						if(appendArgs === true ){
							callArgs = slice.call( arguments, 0 );
							callArgs = callArgs.concat( args );
						}
						return method.apply( scope || window, callArgs );
					};
			};

			/**
			 * Returns a function that allow for afunction to accept either a (key, value )signature or a ({key1:value1, key2:value2}) signature
			 * @paran {Function} fn the function to overload
			 * @return {Function} a function wrapper around the original function
			 */
			exports.overloadSetter =  function( fn ){

				return function overloadedSetter( keyOrObj, value ){

					if( typeof keyOrObj !== 'string' ){
						for( var key in keyOrObj ){
							fn.call( this, key, keyOrObj[key ]);
						}
					}else{
						fn.call(this, keyOrObj, value );
					}
					return this;
				};
			};

			/**
			 * similar to overloadSetter, but used for retrieving values. The new function will accept a ( v1, v2,v3 ) signature or a ([v1,v2,v3]) signature
			 * @param {Function} fn The function to overload
			 * @param {Boolean} plural
			 *
			 */
			exports.overloadGetter =  function( fn, plural ){
				return function ( a ){
					var ret = {}
						,args
						,_fn = fn;
					if( typeof a != 'string'){
						args = a;
					} else if ( arguments.length > 1 ){
						args = arguments;
					} else if ( plural ){
						args = [a];
					}
					if( args && args.length ){
						for( var x = 0; x< args.length; x ++ ){
							ret[ args[x] ] = _fn.call(_fn,  args[x]);
						}
					} else{
						return _fn.call(_fn, a);
					}

					return ret;
				};
			};

			exports.simpleGetter =  function( fn ){
				return function overloadedGetter(key) {
				        var me = this;
				        if (arguments.length > 1) {
				            return Array.from(arguments).map(function(name) {
				                return fn.call(me, name);
				            });
				        } else {
				            return fn.call(this, key);
				        }
				    };
			};
			/**
			 * Allows for a setup function to be called before the original function. Setup function must return a function
			 * @param {Object} obj The object modify
			 * @param {String} key The key in the object to lazify
			 *
			 */
			exports.lazy =  function( obj, key, setup ){
				obj[key] = function(){
					obj[key] = setup.apply( this, arguments );
					obj[key].apply( this, arguments );
				};
			};

			/**
			 * Given a function and some know params, will rturn a function that will be executed with additional arguments appended. Useful when not all parameters are known
			 * @param {Function} fn the function to use
			 * @param {Mixed} args Arbitrary known arguments
			 *
			 */
			exports.partial =  function( fn ){
				var args = Array.from( arguments );
				args.shift();
				return exports.bind.apply( this, [fn, undefined].append(args) );
			};

			/**
			 * marks as function as hidden internally
			 * @param fn {Function} the function to mark
			 * @return {Function} the modified function
			 *
			 */
			exports.hide =  function( fn ){
				fn.$hidden = true;
				return fn;
			};
			/**
			 * marks a function as protected internally
			 * @param {Function} fn the function to mark
			 * @return {Function} the modified function
			 *
			 */
			exports.protect =  function( fn ){
				fn.$protected = true;
				return fn;
			};
			/**
			 * Returns a function to be called at a later time with the provided arguments
			 * @param {Function} fn The function to operate on
			 * @param {Array} args The arguments to pass in to the function
			 * @param {Object} bind the context to bind the function to during execution
			 * @return {Function} A function that will envoke the original function with the previously passed in arguments.
			 */
			exports.pass = function( fn, args, bind ){
				if( args != null ){
					args = Array.prototype.slice.call( args );
				}
				return function(){
					return fn.apply( bind, args || arguments );
				};
			};

			exports.periodical =  function( fn, period, bind, args ){
				var id = setInterval( this.pass(fn, args ,bind), period );

				return id;
			};

			/**
			 * return the value of the first successfull function call of the passed in functions
			 * @param {Function} an arbitrarty number of functions to test
			 * @return {Object} the value of the firs function to execute without error, If there is not a successfull attempt, null will be returned
			 */
			exports.attempt =  function(){
				for (var i = 0, l = arguments.length; i < l; i++){
					try {
						return arguments[i]();
					} catch (e){}
				}
				return null;
			};

			exports.intercept = function( orig, fn, bind ){
				if(typeof fn != 'function'){
					return orig;
				}else{
					return function( ){
						var that = this
							,args = arguments;

						fn.target = that;
						fn.method = orig;

						return ( fn.apply( bind, args ) !== false  ? orig.apply( that, args )  : null);
					};
				}
			};
		exports.extend = exports.overloadSetter( _extend );
	}

);
