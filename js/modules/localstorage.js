/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * Cross browser localStorage functionality with a common API. Also auto serializing and deserializing values for you
 * @module localstorage
 * @author Eric Satterwhite
 * @requires json
 * @requires signals
 **/
define(["require", "exports", "module", ], function(require, exports, module){
	var storage          // placeholder for the browser implememntation of local storage
		,hasLocalStorage // function to determine if the browser has localStorage
		,hasGlobalStorage// function to determin if the brwoser has older globalStorage
		,getter          // internal function to get an item from storage
		,setter          // internal function to set an item in storage
		,deleter         // internal function to delete an item from storage
		,emitter         // an instance of class.Events
		,storageContainer// activeX component for IE fallback
		,storageOwner    // roo document element for IE fallback
		,UNDEF           // placeholder for undefined
		,ieKeyFix        // function to strip invalid chars from storage keys for IE fallabck
		,withIEStorage   // function to wrap internal function for IE fallback
		,LOCAL_STORAGE = "localStorage"
		,GLOBAL_STORAGE = "globalStorage"
		,json = JSON
		,hasAddBehavior = (typeof document === 'undefined') ? false : !!document.documentElement.addBehavior
		,forbiddenCharsRegex = new RegExp("[!\"#$%&'()*+,/\\\\:;<=>?@[\\]^`{|}~]", "g");


	emitter = new Events();

	hasLocalStorage = function(){
		try{

			return (LOCAL_STORAGE in window && window[ LOCAL_STORAGE ] );
		} catch( e ){
			return false;
		}
	};

	hasGlobalStorage = function(){

		try{
			return (GLOBAL_STORAGE in window && window[GLOBAL_STORAGE] && window[GLOBAL_STORAGE][window.location.hostname]);
		} catch( e ){
			return false;
		}
	};


	// HTML5
	if( hasLocalStorage() ){

		storage = window[ LOCAL_STORAGE ];

		getter = function( key ){
			return window[ LOCAL_STORAGE ].getItem( key );
		};

		setter = function(key, val){
			if( val === UNDEF ){
				deleter( key );

			}

			window[ LOCAL_STORAGE ].setItem( key, val  );

			/**
			 * Referes to the number of keys found in storage
			 * @type Number
			 **/
			exports.length = storage.length;

		};

		deleter = function(key){

			window[ LOCAL_STORAGE ].removeItem( key );

			exports.length = storage.length;

		};

		exports.clear = function( ) {
			storage.clear();
			exports.length = 0;
		};

		exports.all = function(){
			var ret = {}
				,key;

			for( var x = 0; x < storage.length; x++ ){
				key = storage.key( x );
				ret[key] = json.decode( storage[key] );
			}

			return ret;
		};


	// Legacy FireFox
	} else if( hasGlobalStorage() ){
		storage = window[GLOBAL_STORAGE][window.location.hostname];

		getter = function(key){
			return storage[key] && storage[key].value;
		};

		setter = function(key, val ){
			if( val === UNDEF ){
				deleter( key );
			}

			storage[key] = val;
			exports.length = storage.length;
		};

		deleter = function( key){
			delete storage[key];
			exports.length = storage.length;
		};


		/**
		 * deletes all values from storage
		 **/
		exports.clear = function( ) {
			for( var key in storage ){
				delete storage[key];
			}
			exports.length = 0;
		};

		/**
		 * returns all of the items found in storage
		 * @return {Object} all found items from storage
		 **/
		exports.all = function(){
			var ret = {}
				,key;

			for( key in storage){
				ret[key] = json.decode( storage[key] );
			}

			return ret;
		};

	  // IE FallBack
	} else if( hasAddBehavior ){
		try{
			// ya. that's right
			storageContainer = new ActiveXObject("htmlfile");
			storageContainer.open();
			// script tags are often times close unless you escape or splite the word script.
			// ya its word
			storageContainer.write('\x3Cscript>document.w=window\x3c/script><iframe src="/favicon.ico"></iframe>');
			storageContainer.close();

			storageOwner = storageContainer.w.frames[0].document;

			storage = storageOwner.createElement("div");
		} catch( e ){
			storage = document.createElement('div');
			storageOwner = document.body;
		}

		withIEStorage = function( fn ){
			return function(){
				var args = Array.prototype.slice.call( arguments, 0);

				args.unshift( storage );


				storageOwner.appendChild( storage );
				storage.addBehavior("#default#userData");
				storage.load( LOCAL_STORAGE );

				var result = fn.apply( exports, args );
				try{

					storageOwner.removeChild( storage );
				} catch( e ){  }

				return result;
			};
		}; // end withIEStorage

		ieKeyFix = function( key ){
			return key.replace( forbiddenCharsRegex, "____" );
		}; // end isKeyFix

		getter = withIEStorage(function( storage, key){
			key = ieKeyFix( key );
			return json.decode( storage.getAttribute( key ) );
		});

		setter = withIEStorage(function( storage, key, val){
			key = ieKeyFix( key );

			if( val === UNDEF ){
				return deleter( key );
			}

			storage.setAttribute( key, json.encode( val ) );
			storage.save( LOCAL_STORAGE );

			return val;

		});
		deleter = withIEStorage(function(storage, key ){
			key = ieKeyFix( key );

			storage.removeAttribute( key );
			storage.save( LOCAL_STORAGE );
		});


		exports.clear = withIEStorage(function( storage ) {
			var attrs = storage.XMLDocument.documentElement.attributes;

			for( var x = 0; x < attrs.length; x++ ){

				storage.removeAttribute( attrs[x].name  );
				storage.save( LOCAL_STORAGE );

			}
			exports.length = 0;
		});


		exports.all = withIEStorage(function( storage ){
			var attrs = storage.XMLDocument.documentElement.attributes
				,ret = {};
			for( var x = 0; x <= attrs.length; x++ ){

				ret[ attrs[x].name ] = exports.get( attrs[x].name );
			}
			return ret;
		});


	// Dummy Storage Backend
	} else{

		getter = setter = deleter = function(){};
	}

	/**
	 * sets key value pair in storage
	 * @param {String} key ...
	 * @param {Object} value ...
	 **/
	exports.set = function(key,value){

		var val = typeof value === 'string' ? value : json.encode( value );
		var old_val = getter( key );

		var changed = val != old_val ? true : false;

		setter( key, val );

		if( changed ){

			var evt = {
				key: key,
				oldValue: typeof old_val === 'string' ? old_val : json.decode( old_val ),
				newValue: val,
				url:window.location.href
			};


			emitter.fireEvent( "storage", evt );
		}
	}; // void

	/**
	 * Attempts to retrieve an item out of storage
	 * @param {String} key the key of the value to retrieve from storage
	 * @return {Object} the object that was found at the key or null
	 **/
	exports.get = function( key ){

		var val = getter( key );


		try{
			return json.decode( val );
		} catch( e ){
			return val;
		}
	};

	/**
	 * Delets an item found in storage
	 * @param {String} key The key of the item to delete from storage
	 **/
	exports.remove = function( key ){
		var old_val = getter( key );


		deleter( key );


		var evt = {
			key: key,
			oldValue: json.decode( old_val ),
			newValue: null,
			url:window.location.href
		};


		emitter.fireEvent( "storage", evt );
	}; // void


	/**
	 * Binds a handler function to the storage event wich is fired when an item is added or removed from storage
	 * @param {Function} handler the handler to bind
	 **/
	exports.addEvent = function( fn ){
		emitter.addEvent("storage", fn);
	};

	/**
	 * removes an specific handler from the storage event bindings
	 * @param {Function} handler A reference to the function that was originally bound that is to be removed
	 **/
	exports.removeEvent = function(fn){
		emitter.removeEvent("storage", fn );
	};
});
