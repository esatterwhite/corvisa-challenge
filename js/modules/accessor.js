/*jshint laxcomma:true, smarttabs: true, -W083: true, mootools:true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * accessor is a single function that emables objects to define and lookup abritrary amounts and types of data by name.
 * @module accessor
 * @author Eric Satterwhite
 * @example braveheart(['class', 'accessor'], function( Class, accessor ){
	var MyClass = new Class();

	accessor.call( MyClass, "Thing");

	MyClass.defineThing('random', Math.random() );

	MyClass.defineThing(/([\w]+)\.([\w]+)/, function(){
		console.log( arguments )
	})
	MyClass.defineThings({
		hello:"world"
		,foo:'bar'
	});

	MyClass.eachThing(function( val, key, obj ){
	    alert( val );
	}); // alerts things a lot!

	log.log( MyClass.lookupThing('foo') ) // "bar"

	log.log( MyClass.lookupThing("Hello.World") )// ["Hello", "World"]

})
 * @requires core
 * @requires object
 */

define(function( require, exports){

       return function(singular, plural) {
		var def = 'define'
	        ,lookup = 'lookup'
	        // ,match = 'match'
	        ,each = 'each'
	        ,possible = "possible"
	        ,accessors = '__accessor' + singular
	        ,matchers = '__matchers' + singular
	        ,defineSingular
	        ,definePlural
	        ,lookupSingular
	        ,lookupPlural
	        ,eachSingular
	        ,possiblePlural;

		this[accessors] = {};
	    this[matchers]= [];

		if (!plural) {
	        plural = singular + 's';
	    }


	    /**
	     * Objects will be embued with a defineFOO method and a defineFOOs where FOO matches the name you passed in as the name
	     * @type {Function}
	     * @param {String|RegEX} key The name of the key to store. If passed a regex, it will be used to match possible keys.
	     * If the object associated with the matched key is a function, the function will be passed in the matches as arguments when the function is called.
	     * @param {Object} value What ever you want to stash away
	     * @return {Object} The original Object passed in
	     */
		exports.defineFOO = function(key, value) {
			if (typeOf(key) === 'regexp') {
	            this[matchers].push({'regexp': key, 'value': value, 'type': typeOf(value)});
	        } else {
	            this[accessors][key] = value;
	        }
			return this;
		};
		defineSingular = this[def + singular] = exports.defineFOO;

	    /**
	     * Objects will be embued with a defineFOO method and a defineFOOs where FOO matches the name you passed in as the name
	     * @param {Object} pairs key/value pairs to run through the defineFOO method as normal
	     * @return {Object} The original Object passed in
	     */
		exports.defineFOOs =  function(obj) {
			for (var key in obj) {
	            this[accessors][key] = obj[key];
	        }
			return this;
		};

		definePlural = this[def + plural] = exports.defineFOOs;
		/**
	     * Objects will be embued with a lookupFOO method and a lookupFOOs where FOO matches the name you passed in as the name
	     * @param {String} key the The key of what you would like to look up
	     * return {object} value The value if found, or null.
	     */
		exports.lookupFOO =  function(key) {
			if (key in this[accessors] && this[accessors].hasOwnProperty( key )) {
	            return this[accessors][key];
	        }
			for (var l = this[matchers].length; l--; l) {
				var matcher = this[matchers][l], matched = String(key).match(matcher.regexp);
				if (matched && (matched = matched.slice(1))) {
					if (matcher.type === 'function') {
	                    return function() {
	                        return matcher.value.apply(this, [].slice.call(arguments).concat(matched));
	                    };
	                } else {
	                    return matcher.value;
	                }
				}
			}
			return null;
		};

		lookupSingular = this[lookup + singular] = exports.lookupFOO;

		/**
	     * Objects will be embued with a defineFOO method and a defineFOOs where FOO matches the name you passed in as the name
	     * @param {String}  key A comma separated list of key you wish to lookup
	     * return {Object} an object of key/value pairs matching the passed in arguments
	     */
		exports.lookupFOOs =  function() {
			var results = {};
			for (var i = 0; i < arguments.length; i++) {
				var argument = arguments[i];
				results[argument] = lookupSingular.call(this,argument);
			}
			return results;
		};

		lookupPlural = this[lookup + plural] = exports.lookupFOOs;

		/**
	     * Lists the  names of defined properties aavaliable to the accessor
	     * return {Array} an array of defined FOOs
	     */
		exports.possibleFOOs =  function() {

			return Object.keys(this[accessors]);
		};

		possiblePlural = this[possible + plural] = exports.possibleFOOs;

		/**
		 * Executes a function on every key/value pair stored
		 * @param {Function} fn The function to execute
		 * @param {Object} bind The object in which context to execute the function
		 */
		exports.eachSingular =  function(fn, bind) {
			Object.each(this[accessors], fn, bind);
		};

		eachSingular = this[each + singular] = exports.eachSingular;
	};
});
