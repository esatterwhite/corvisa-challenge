/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * View Tests for challenge
 * @module module:test/view
 * @author Eric Satterwhite
 **/
define(['require'
	,'module'
    ,'exports'
    ,'mvc/view'
    ,'mootools'
    ,'mooml'
],function(require, module, exports, View){

	exports.sanity = {
		setUp:function( callback ){


			callback()
		}

		,tearDown: function( callback ){

			document.id('testdiv').empty()
			callback()
		}

		,"Sanity":{
			"mootools": function( test ){
				test.ok( MooTools, "mootools is loaded")
				test.done()
			}

			,"mooml":function( test ){
				test.ok(Mooml, "template engine loaded")
				test.done()
			}

			,"View": function( test ){
				test.ok(View)
				test.done()

			}
		}
	};


	
	exports.views= {
		setUp: function( callback ){
			callback()
		}
		,tearDown: function( callback ){
			document.id('testdiv').empty();
			callback()
		}
		,"View Instance":{

			"Data as options":function( test ){

				var view = new View({
					tpl: function(data){
						var tag = Mooml.engine.tags;

						tag.div({"class":"test", "id":"testdiv"}
							,tag.div({"class":"output"},"hello " + data.text )
						)
					}
					,data:{text:"world"}
					,renderTo:'testdiv'
				});


				view.render();

				test.ok( view.element );
				test.equal( view.element.get('text'), "hello world", "view should render 'hello world'");

				var el = $(view)
				test.equal( typeOf(el), "element", '$( view ) should return its root element')

				test.done();
			}

			,"Nested Views": function( test ){
				var ul = new View({
					renderTo:'testdiv'
					,override:false
					,tpl: function(){
						var tags = Mooml.engine.tags;
						tags.ul()		
					}
				});
				test.equal($(ul).getChildren('li').length, 0, "initial ul element should be empty" )
				var li = new View({
					tpl:function( data ){
						var tags = Mooml.engine.tags;
						tags.li('hello world')
					}
				})

				ul.add( li )
				test.equal($(ul).getChildren('li').length, 1, "#add should render a sub template to its parent template")
				var li2 = new View({
					tpl:function( data ){
						var tags = Mooml.engine.tags;
						tags.li('hello agint')
					}
				})

				ul.add(li2)
				test.equal($(ul).getChildren('li').length, 2, "#add should render a sub template to its parent template")
				test.done();
			}
		}
	};
	exports.multi={
		setUp: function( callback ){
			callback()
		}

		,tearDown: function( callback ){
			document.id('testdiv').empty()
			callback();
		}

		,"Templates from array": function( test ){
			var ul = new View({
				renderTo:'testdiv'
				,override:false
				,tpl: function(){
					var tags = Mooml.engine.tags;
					tags.ul()
				}
			});
			test.equal($(ul).getChildren('li').length, 0, "inital ul node should be empty")

			var list = new Class({
				Extends:View
				,options:{
					override:false
					,tpl: function( data ){
						Mooml.engine.tags.li("")
					}
					
				}
			});
			var l = new list({
				data:[{
					test:"hello one"
				}
				,{
					test:"hello two"
				}
				,{
					test:"hello three"
				}]
			})

			ul.add( l )
			var found = $(ul).getChildren('li').length
			test.equal(found, 3, "Template should render as many nodes as data elements")
			test.done()
		}
	};
});