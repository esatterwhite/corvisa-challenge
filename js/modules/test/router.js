/*jshint laxcomma:true, smarttabs: true */

define(["require", "exports", "module","mvc/router", "mvc/controller"],function(require, exports, module, Router, Controller){
	exports.reverse = {
		setUp: function( callback ){
			this.router1 = new Router();

			Controller.defineControllers({
				"simple.controller": {
					method1: function( ){
						return "method1";
					}

					,method2: function( ){
						return "method2";
					}
					,method3: function( param){
						return ""+param;
					}
				}
			});


			this.router1.update({
				routes:{
					"some/controller/one":{controller:"simple.controller", action:"method1"}
					,"some/controller/two":{controller:"simple.controller", action:"method2"}
					,"some/controller/{three}":{controller:"simple.controller", action:"method3"}
				}
			});

			callback()
		}

		,tearDown: function( callback ){
			delete this.router1;
			callback();
		}

		,"Single Routes": function( test ){
			test.expect( 3 );

			test.equal( this.router1.reverse("simple.controller", "method1"), "some/controller/one");
			test.equal( this.router1.reverse("simple.controller", "method2"), "some/controller/two");
			test.equal( this.router1.reverse("simple.controller", "method3", {"three":3}), "some/controller/3");

			test.done()
		}
	};

	exports.dispatching = {
		setUp: function( callback ){
			this.router2 = new Router({
				filters:[
				]
			});
			this.router2.enable();
			Controller.defineControllers({
				"argument.controller":{
					method1: function(arg1, arg2, arg3, t ){
						t.strictEqual( arg1, 1)
						t.strictEqual( arg2, 2)
						t.strictEqual( arg3, 3)

						t.done()
					}
					,method2: function(  ){

					}
				}
				,"object.controller":{
					method1: function( opts){
						t = opts.t;
						t.equal( typeof opts.str, "string");
						t.equal( typeof opts.num, "number");
						t.equal( typeof opts.fn, "function");
						t.done();

					}
					,method2: function( ){

					}
				}
			});

			this.router2.update({
				routes:{
					"things/{value}":{controller:"argument.controller", action:"method1"}
					,"stuff/{value}":{controller:"argument.controller", action:"method2"}

				}
			})

			this.router2.update({
				routes:{
					"duder/:id:":{controller:"object.controller", action:"method2"}
					,"lame/test":{controller:"object.controller", action:"method1"}
				}

			});

			callback();
		}
		,tearDown: function( callback ){
			delete this.router2;
			callback();
		}

		,"Multi Args": function( test ){
			test.expect( 3 )

			this.router2.dispatch({
				controller:"argument.controller"
				,action:"method1"
				,args:[1, 2, 3, test ]
			});

		}
		,"Object Passing": function( test ){
			this.router2.dispatch({
				controller:"object.controller"
				,action:"method1"
				,str:"Hello"
				,num: 3
				,fn: function(){}
				,t:test
			})
		}

		,redirecting: function( test ){

			var args
			this.router2.redirect( "stuff/test" );
			test.equal( this.router2.getHash(), "stuff/test");

			args = this.router2.getHashAsArray();
			test.equal( args[0], "stuff");
			test.equal( args[1], "test");

			this.router2.redirect( "duder/test" )
			args = this.router2.getHashAsArray();
			test.equal( args[0], "duder");
			test.equal( args[1], "test");

			this.router2.redirect( "duder/1" )
			args = this.router2.getHashAsArray();
			test.equal( args[0], "duder");
			test.strictEqual( parseInt( args[1], 10 ), 1);

			this.router2.redirect( "duder" )
			args = this.router2.getHashAsArray();
			test.equal( args[0], "duder");
			test.equal( args[1], undefined );

			test.done();
		}
	};

	exports.regex = {
		setUp: function( callback ){
			this.router = new Router({
				routes:{
					"^\/[0-9]+\/([0-9]+)$":{
						regex:true
						,controller:"expression.controller"
						,action:"regexhandler"
					}

					,"^\/[\-\\w]+\/([0-9]+)\/([\\w]+)$":{
						regex:true
						,controller: "expression.controller"
						,action:"grouphandler"
					}
				}
			});

			Controller.defineControllers({
				"expression.controller":{
					regexhandler: function( grouping ){
						console.log( arguments );

						if( typeof grouping !== "number"){
							throw "expected number, got " + ( typeof grouping )
						}
					}
					,grouphandler: function( nums, chars){
						console.log( nums, chars );

						if( typeof nums !== "number"){
							throw "expected number, got " + ( typeof grouping )

						}

						if( typeof chars !== "string" ){
							throw "expected string, got " + ( typeof grouping )

						}
					}
				}
			})
			callback();
		}

		,"Expression single group": function( test ){
			var that = this;
			test.doesNotThrow( function( ){
				that.router.dispatch( "/123/456")
				that.router.dispatch( "/123/678")
			})
			test.done()
		}

		,"Expression Multi Group": function( test ){
			var that = this;

			test.doesNotThrow( function(){
				that.router.dispatch("/hello/123/world")
			});

			test.done();
		}


	};

	exports.complex = {
		setUp: function( callback ){
			var counter;
			this.counter = counter = 0;
			this.router4 = new Router({
					routes:{
						"{lorem}/{foo}":{
							controller:"pattern.controller"
							,action:"loremhandler"
							,rules:{
								lorem: /^lorem/
								,foo:/([a-z]+)$/

							}
						}
						,"after{?query}": {
							controller:"pattern.controller"
							,action:"queryhandler"

						}
						,"rest/{id*}/edit":{
							controller:"pattern.controller"
							,action:"restHandler"
						}
						,"query/:id*:/:?query:":{
							controller:"pattern.controller"
							,action:"queryresthandler"
						}						
					}
			});
			Controller.defineControllers({
				"pattern.controller":{
					loremhandler: function( betterBeLorem  ){
						if( betterBeLorem !== "lorem" ){
							throw "I Aint Lorem Fool!"
						} else{
							return true;
						}
					}
					,queryhandler: function( query ){
						if( query.do != "stuff" ){
							throw "Send some params fool!"
						}
					}

					,restHandler: function( args ){
						var bits = args.split("/");

						var resource = bits[0];
						var id = bits[1];

						if( typeof parseInt( id, 10 ) != "number"){
							throw "I expect a number!";
						}

					}
					,queryresthandler: function( r, q ){
						console.log( arguments )
					}

				}
			})
			this.router4.enable();


			callback();
		}
		,tearDown: function( callback ){
			delete this.router4;
			callback();
		}
		,"Pattern Matching": function( test ){
			var router = this.router4;

			test.doesNotThrow(function(){
				router.parse( "/lorem/foo" )
			});
			test.doesNotThrow(function(){
				router.parse( "/lorem/TED" )
			});
			test.doesNotThrow( function( ){
				router.parse("after?do=stuff")
			});


			test.done()
		}
		,"Rest Style Routes": function( test ){

			var that = this;

			test.doesNotThrow( function( ){
				that.router4.redirect("rest/thing/10/edit")
			})
			test.ok( true )
			test.doesNotThrow( function( ){
				that.router4.redirect("rest/test/11/edit")
			})
			test.ok( true )

			test.doesNotThrow( function( ){
				that.router4.redirect("rest/sample/1/edit")
			})

			test.doesNotThrow( function( ){
				that.router4.redirect("query/sample/13/bypass?foo=bar")
			})
			
			test.ok( true )

			test.done( )
		}
	}

	exports.filters = {
		setUp: function( callback ){
			this.filter = function( ){
				return false;
			};

			this.router = new Router({
					routes:{
						"filtered/:id:": {
							controller:"filter.controller"
							,action:"meth1"

						}
						,"rest/{id*}/edit":{
							controller:"pattern.controller"
							,action:"restHandler"
						}
						,"query/{rest*}:?q:":{
							controller:"pattern.controller"
							,action:"queryresthandler"
						}
					}
					,filters:[
						this.filter
					]

			});

			Controller.defineControllers({
				"filter.controller":{
					meth1: function( t ){
						t.ok( 1 );
						t.done();
					}

				}
			});

			window.router = this.router
			callback();
		}

		,tearDown: function( callback ){
			delete this.router;
			callback();
		}

		,"Filtered": function( test ){
			test.expect( 0 )
			this.router.dispatch({
				controller:"filter.controller"
				,action:'meth1'
				,args:[test, 1, 2, 3 ]
				// ,force: true
			})
			test.done();
		}
		,"Remove A  Filter": function( test ){
			test.expect( 1 )
			this.router.dispatch({
				controller:"filter.controller"
				,action:'meth1'
				,args:test
			})

			this.router.removeFilter( this.filter );
			this.router.dispatch({
				controller:"filter.controller"
				,action:'meth1'
				,args:test
			})
		}
		,"Filter Events": function( test ){
			test.expect( 2 )

			this.router.dispatch({
				controller:"filter.controller"
				,action:"meth1"
				,args:test
			})
			this.router.on("routefiltered", function( ){
				test.ok( true, "Event should be dispatched when a route is filted" )
				this.router.dispatch({
					controller:"filter.controller"
					,action:"meth1"
					,args:test
					,force: true 
				})
			}.bind( this ));

			this.router.redirect( "filter/1")
		}
	};

	exports.linking = {

		setUp: function( callback ){
			this.router = new Router({
				ignoreState:false
				,routes:{
					"/foo/:things*:":{controller:"link.controller", action:"dual"}
				}
			});

			this.otherRouter = new Router({
				routes:{
					"/foo/bar/:name::?things:":{ controller:"link.controller", action:"alternate"}
				}

			});

			Controller.defineControllers({
				"link.controller":{
					dual: function( t ){
						console.log( "#### original router")
					}
					,alternate: function( t ){
						console.log( "#### linked router")

					}
				}
			});
			callback()
		}

		,tearDown: function( callback){
			delete this.router;

			callback();
		}

		,"Router Linking": function( test ){

			this.router.link( this.otherRouter )
			this.router.dispatch("foo");
			console.log("")

			this.router.dispatch("foo/bar");
			console.log("")

			this.router.dispatch("foo");
			console.log("")

			this.router.dispatch("foo/bar");
			this.router.dispatch("foo");
			console.log("")

			this.router.dispatch("foo/bar");
			this.router.dispatch("foo/bar/cow");
			console.log("")

			test.done();
		}

		,"Router Unlinking": function( test ){
			test.done();
		}

		,"Crossroads Links": function( test ){

			test.done();
		}
	};

	exports.recognize = {
		setUp: function( callback ){

			this.router = new Router({

				routes:{
					"^\/[0-9]+\/foo/([0-9]+)$":{
						 regex:true
						,controller:"recognize.controller"
						,action:"thingies"
					}
					,"^\/[0-9]+\/foo/([0-9]+)/done$":{
						 regex:true
						,controller:"recognize.controller"
						,action:"killers"
					}
				}
			})

			Controller.defineControllers({
				"recognize.controller":{
					thingies: function( t ){
						t.ok( 1 )
						console.log( arguments )
					}
					,killers: function( t ){

						t.ok( 1 );
						t.done();
					}
				}
			})

			callback()
		}

		,tearDown: function( callback ){

			callback();
		}
		,"Betta Recuniz": function( test ){
			test.expect( 2 )
			var x = this.router.recognize("/1/foo/2");
			x.args = [test]
			this.router.dispatch( x );

			var x = this.router.recognize("/1/foo/12/done");
			x.args = [test]

			this.router.dispatch( x );
		}

	}

});
