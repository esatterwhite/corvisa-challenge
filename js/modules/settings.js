/*jshint laxcomma:true, smarttabs: true */
/**
 * Provides settings functionality
 * @module module:settings
 * @author Eric Satterwhite
 * @requires localstorage
 **/
 define([
	'require'
	,'module'
	,'exports'
	,'mvc/router'
	,'mvc/controller'
	,"settings/views"
	,"settings/urls"
	,"settings/controllers"
 ], function(require, module, exports, Router, Controller, views, urls, controllers ){


		Controller.defineControllers( controllers );
		var appRouter = new Router({
			routes: urls
		});

		router.link( appRouter );
	 }
 );
