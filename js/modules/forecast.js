/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * forecast application harness
 * @module forecast
 * @author Eric Satterwhite
 * @requires module:mvc/controller
 * @requires module:mvc/router
 * @requires module:mvc/controllers
 **/
define([
	"require"
	, "module"
	, "exports"
	, "mvc/controller"
	, "mvc/router"
	, "forecast/urls"
	, "forecast/controllers"
],function(require, module, exports, Controller, Router, urls, controllers){
		Controller.defineControllers( controllers );
		var appRouter = new Router({
			routes:urls
		});

		router.link( appRouter );
	}
);
