/*jshint laxcomma:true, smarttabs: true,  mootools: true */
/**
 * Provides an abstraction around hashtag based url routing
 * @module Router
 * @author Eric Satterwhite
 * @requires class
 * @requires accessor
 * @requires crossroads
 * @requires module:data.Controller
 * @requires functools
 * @requires array
 * @requires hasher
 * @requires object
 **/
define([
	"require"
	, "exports"
	, "module"
	, "mootools"
	, "./controller"
	, "crossroads"
	, "functools"
	, "hasher"
	]
	,function(require, exports, module, Mootools, Controller, crossroads, functools, hasher){
		var  Router;
	 
		function arrayinsert( arr, item, idx ){
			idx = (idx > 0) ? idx : 0;

			return arr.splice( idx,0, item);
		}

		//Controller.defineController("test", {
		//	doTest: function( ){
		//		console.log( arguments )
		//	}
		//})


		/**
		 * Class responsibe for mapping route patterns to controller functions. Also manages brower history and dispatches URLs based on hash changes;
		 * @class module:data.Router
		 * @param {Object} options Configurations optiosn for the router
		 * @param {Boolean} [options.typeCast=true] If set to true, paremeters found in urls will be type cast before sent to controller actions
		 * @param {Object} [options.routes={}] An object mapping URLs to controller/ action pairs
		 * @param {Array} [options.filters=[]] An array of functions that determin if a route should actually be dispatched. If an function in the array returns false, The route will not be executed
		 * @example var x = new NAME.Thing({});
		 */
		Router = new Class(/** @lends module:data.Router.prototype */{
			Implements:[ Options, Events]
			,options:{
				typeCast:true
				,routePrefix:"/"
				,ignoreState:true
				,defaultRoute:null
				,routes:{
					// "something/:id:/":{ controller:"test" , action: "doTest"}
				}

				,filters:[ ]
			}

			,$routes:{}
			,$linkedRouters: []
			,$isLinked: false

			,initialize: function( options ){
				var routes;

				this.setOptions( options );
				routes = this.options.routes;

				this.$router = crossroads.create();
				this.$router.shouldTypecast = this.options.typeCast;
				this.$router.ignoreState = !!this.options.ignoreState;

				hasher.prependHash = this.options.routePrefix;
				//Listen only if we are not linked
				if(!this.options.link) {
					hasher.initialized.add( functools.bind(this.parse, this ) );
					hasher.changed.add( functools.bind(this.parse, this ) );
				} else {
					options.link.link(this);
				}


				for( var key in routes ){
					this.map(key, routes[key] );
				}



				this.started = false;
				// hasher.init();

			}


			/**
			 * determines if the router should execut the controller action based on configured filters.
			 * if one of the functions throws and error or returns false, the dispatcher will not execute the controller action
			 * @method module:data.Router#check
			 * @private
			 * @param {TYPE} NAME ...
			 * @return {Boolean} passed true if all of the filters passed.
			 **/
			,check: function( force, args, rte  ){
				var pass
					,filters;
				// ## Filters
				// Filters are used to determin if the router should continue dispatching the matched route.
				// Because the routers even system is actually dis connected from the url parser it is not possible
				// to stop the router based on the standard `Class.Events` system.

				if( !!force ){
					return true;
				}

				pass = true;
				filters = this.options.filters;
				for( var x=0,len=filters.length; x<len; x++ ){

					try{
						pass = filters[x].apply(null, args);
						if( !pass ){
							break;
						}

					} catch( e ){
						pass = false;
						break;
					}
				}
				if( !pass ){
					this.fireEvent("routefiltered", [this, args, rte ]);
				}
				return pass;
			}.protect()

			/**
			 * Configures the matching route with the supplied rules, preserving existing rules
			 * @chainable
			 * @method module:data.Router#configure
			 * @param {String} pattern original pattern of the registered route you wish to update
			 * @param {object} [rules] any rules to apply to the route
			 * @returns {Router} The current router instance
			 **/
			,configure:function( route, cfg ){

				var name = typeof route === "string" ? route : route._pattern;

					cfg = cfg || {} ;

				if(!name){
					return;
				}

				var rte = this.$routes[ route ];
				rte.rules  = rte.rules ? Object.merge( rte.rules , cfg) : cfg;

				return this;
			}
			/**
			 * Stops the router from watching the hash fragement changes
			 * @chainable
			 * @method module:data.Router#disable
			 * @return {module:data.Router} router instance
			 **/
			,disable: function( ){
				hasher.changed.active = false;
				return this;
			}

			/**
			 * enables the router to watch the hash fragement changes
			 * @chainable
			 * @method module:data.Router#enable
			 * @return {module:data.Router} Router instance
			 **/
			,enable: function( ){
				if( !this.started ){
					if( this.options.defaultRoute && !window.location.hash ){
						hasher.setHash( this.options.defaultRoute );
					}

					hasher.init();
					this.started = true;
				}
				hasher.changed.active = true;
				return this;
			}
			/**
			 * If supplied a string, it will be parsed and dispatch the first matching route. If given and object, it will lookup a controller and execute the matching action passing the original object as the only argument
			 * @method module:data.Router#dispatch
			 * @param {String|Controller|Object} Route
			 **/
			,dispatch: function( rte, args ){
				var routes
					,routeType
					,force_dispatch;

				routeType = typeof rte;
				routes = this.$routes;
				force_dispatch = routeType === "string" ? false : rte.force;
				args = Array.from( rte.args || rte );

				if( this.check( force_dispatch, args, rte ) ){
					if( routeType === "string"){
						this.parse( rte, args );
					} else{
						this.execute( rte );
					}
				}

				return this;
			}

			/**
			 * does the work of finding a matching controller and callign a funcdtion
			 * @protected
			 * @method module:data.Router#execute
			 * @param {Object} options
			 **/
			,execute: functools.protect(function( /** Object */ opts ){
				var options = opts
					,url = options.url
					,ctrl
					,act
					;

				ctrl = (typeof options.controller === "string") ? Controller.lookupController( options.controller ) : options.controller;

				act = ctrl[options.action];

				if( url ){

					if( typeof url === "string"){
						hasher.changed.active = false;
						hasher.setHash( url );
						hasher.changed.active = true;
					}else{
						throw "";
					}
				}

				if( typeof act === "function" ){
					if( opts.args ){
						act.apply( ctrl, Array.from(opts.args) );
					} else {
						act.call( ctrl, opts );
					}
					return;
				}
			})

			/**
			 * Retrieves the base url of the current document
			 * @method module:data.Router#getBaseUrl
			 * @return {String} The url
			 **/
			,getBaseUrl: hasher.getBaseURL

			/**
			 * returns the url of the current document after the hashtag
			 * @method module:data.Router#getHash
			 * @return {String} Hash fragement
			 **/
			,getHash: hasher.getHash

			/**
			 * returns the full url of the current docuemtn
			 * @method module:data.Router#getUrl
			 * @return {String} The current Url
			 **/
			,getUrl: hasher.getURL

			/**
			 * Returns splits the has fragment on /, and returns the peices
			 * @method module:data.Router#getHashAsArray
			 * @return {Array} The array of url pieces
			 **/
			,getHashAsArray: hasher.getHashAsArray

			/**
			 * takes a url pattern and a url, and determines if their is a registed route that matches
			 * @method module:data.Router#match
			 * @param {String} patterne
			 * @param {String}
			 * @return {Boolean}
			 **/
			,match: function( pattern, url ){
				var route;

				route = this.$routes[ pattern ];

				if( route ){
					return route.match( url );
				}

				return false;
			}

			/**
			 * This does something
			 * @method module:data.Router#map
			 * @param {Sring} name DESCRPTION
			 * @param {Object} name DESCRIPTION
			 * @returns {module:data.Router} router current router instance
			 */
			,map: functools.overloadSetter(function( rte, opts ){
				var _expression;

				opts = opts || {};

				// If the route has been marked as a regex
				// We pass the URL throughthe RegExp constructor with optional
				// flags other wise pass the URL as is
				if( opts.regex ){
					_expression = new RegExp( rte, opts.flags );
					this.$routes[ rte ] = this.$router.addRoute( _expression );
				} else{
					this.$routes[ rte ] = this.$router.addRoute( rte );
				}

				if( typeof opts.rules == "object" ){
					this.$routes[ rte ].rules = opts.rules;
				}

				this.$routes[ rte ].matched.add( functools.bind(function(){
					var args
						,options;

					args = rte.args || Array.from( arguments );
					options = opts;
					options.args = rte.args || Array.from( arguments );

					this.dispatch( options );

				}, this) );
				return this;
			})

			/**
			 * parses a url and dispatches any matching routes, by passing any hash fragement changes
			 * @method module:data.Router#parse
			 * @param {String} NAME ...
			 * @param {String} NAME ...
			 **/
			,parse:  function(newHash, oldHash, args ){
				if(this.$linkedRouters.length > 0) {
					//If we do have linked routers, delegate first
					var delegated = false;
					for(var routerIdx in this.$linkedRouters) {
						var router = this.$linkedRouters[routerIdx];
						if(router && !delegated  && router.recognize && router.recognize(newHash)) {
							//Check filters before delegating
							if(this.check(false)) {
								router.parse(newHash, null, Array.from( args ));
								delegated = true;
							}
						}
					}

					//If we found no delegate, try to parse it ourselves
					if(!delegated) {
						this.$router.parse( newHash, Array.from( args ) );
					}
				} else {
					//If we have no linked routers, parse it ourselves
					this.$router.parse( newHash, Array.from( args )  );
				}
			}


			/**
			 * sets the url fragement to the passed in string and dispatches any matched routes
			 * @method module:data.Router#redirect
			 * @param {String} url
			 **/
			,redirect: function( ){
				if( this.check(false, arguments, arguments[0] ) ){
					hasher.setHash.apply( hasher, arguments );
				}
			}

			/**
			 * removes a filter or the filter at a specific location
			 * @method module:data.Router#removeFilter
			 * @param {Function|Number} filter If passed a number, the filter at the specified index will be removed. If passed a function, that function will be removed from the list, if found.
			 * @return {Router} The current Router instance
			 **/
			,removeFilter: function( fn ){
				if( typeof fn == "number" ){
					try{
						this.options.filters.splice( fn,1 );
					} catch( e ){

					}
				} else{
					this.options.filters = this.options.filters.erase( fn );
				}

				return this;
			}

			/**
			 * adds a filter to the array of filters, optionally at a specified location
			 * @method module:data.Router#addFilter
			 * @param {Function} fn The function to add to the list of filters
			 * @param {Number} [index] the index to add the filter at
			 * @return {Router} the current router instance
			 **/
			,addFilter: function( fn /* [, index ] */ ){
				if( typeof arguments[1] === "number" ){
					this.options.filters = arrayinsert( this.options.filters, fn, arguments[1] );
				} else{
					this.options.filters.push( fn );
				}

				return this;
			}

			/**
			 * Given a registered url, will return the controller / action pair it maps too. Will return false, if not found
			 * @method module:data.Router#recognize
			 * @param {String} url The url that has been registered to the router instance to look up.
			 * @return {Object} matched pair or null
			 **/
			,recognize: function( url ){
				var routes
					,matched
					,len;

				matched = null;

				// search over the more specific, linked routes first
				var do_break = false;
				len = this.$linkedRouters.length;
				for( var x = 0; x < len; x++ ){
					if( do_break ){
						break;
					}
					var linkedRouter = this.$linkedRouters[ x ];

					routes = linkedRouter.$routes;

					for(var key in routes ){
						matched = routes[ key ].match( url );
						if( matched ){
							matched = linkedRouter.options.routes[ key ];
							do_break=true;
							break;
						}
					}
				}

				// If their was nothing found
				// fallback to the main router instance
				if( !matched ){

					routes = this.$routes;
					for( var key2 in routes ){
						matched = routes[ key2 ].match( url );
						if( matched ){
							matched  = this.options.routes[ key2 ];
							break;
						}
					}
				}

				return matched;
			}

			/**
			 * Given the name of a controller and action mapped to a route, will return the fully qualifed url for the given parameters.
			 * @method module:data.Router#reverse
			 * @param {String} controller The qualified name of a registered controller to lookup
			 * @param {String} the method nam on the controller to match the url to
			 * @param {Object} options an objecdt containing parameters to send to the route to be interpolated
			 * @return {String} the url mapped to the controller and action
			 **/
			,reverse: function( controller, action, options ){
				var  routes
					,cls
					,ctrlType
					,route
					,router
					,len;

				options = options || {};

				len = this.$linkedRouters.length;
				//Get all routes, including linked routers

				ctrlType = typeof controller;

				if( ctrlType === "string" ){
					controller = Controller.lookupController( controller );
				}

				var do_break = false;
				// for each linked router
				for( var x = 0; x < len; x++ ){
					if( do_break ){
						break;
					}

					router = this.$linkedRouters[ x ];
					routes = router.options.routes;

					for( var key in routes ) {
						cls = Controller.lookupController( routes[key].controller );
						if( cls == controller && routes[key].action == action){
							route    = router.$routes[key];

							do_break =true;
							break;
						}
					}

				}
				if(!route){
					for( var key2 in this.options.routes ) {
						cls = Controller.lookupController( this.options.routes[key2].controller );
						if( cls == controller && this.options.routes[key2].action == action){
							route    = this.$routes[key2];
							break;
						}
					}
				}
				if( route ){
					return route.interpolate( options );
				}

				return "";
			}


			/**
			 * Updates the internal router and class with new route configurations
			 * @method module:data.Router#update
			 * @param {Object} config suiteable configuration for the router class.
			 * @return {Router} The current router class
			 **/
			,update: function(  config ){
				this.setOptions( config );
				this.map( config.routes || {} );
				return this;
			}

			/**
			 * Convience Method. Short cut to update
			 * @method module:data.Router#updateRoutes
			 * @param {Object} routes An object containing route configurations
			 * @return {Router} The current router instance
			 **/
			,updateRoutes: function( routes ){
				return this.update({
					routes: routes
				});

			}

			/**
			 * Links another router to this current router allwing route dispatching to potentially different applications
			 * @method module:data.Router#link
			 * @param {Router|BraveHeart}
			 **/
			,link: function( router ){
				// We shoud allow users to pipe from a router
				// to an instance of a data.Router
				if( router instanceof Router ){
					//Add to linked routers list
					router.$isLinked = true;
					this.$linkedRouters.push(router);
				}

				return this;
			}

			/**
			 * Unlins a specific router from the link chain
			 * @method module:data.Router#unline
			 * @param {Router|BraveHeart} router the instance of router
			 **/
			,unlink: function( router ){
				// We shoud allow users to pipe from a router
				// to an instance of a data.Router
				if( router instanceof Router ){
					//Remove from linked routers list
					router.$isLinked = false;
					this.$linkedRouters.erase( router );
				}

				return this;
			}

		});

		return Router;
	}
);
