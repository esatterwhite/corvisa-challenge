/*jshint laxcomma:true, smarttabs: true */
if( typeof define !== 'function'){
    var define = require('amdefine')( module, require );
}
/**
 * DESCRIPTION
 * @module module:data.Controller
 * @author Eric Satterwhite
 * @requires class
 * @requires accessor
 **/
define(["require", "exports", "module", "mootools", "accessor"],function(require, exports, module, Mootools, accessor){
    var  Controller;
 
    /**
     * The controller
     * @class module:data.Controller
     * @param {Object} options
     * @example var x = new NAME.Thing({});
     */
    Controller = new Class(/** @lends module:data.Controller.prototype */{
		Implements:[ Events, Options ]
		,initialize: function(){

		}
    });

	accessor.call(Controller,"Controller");

	return Controller;
});
