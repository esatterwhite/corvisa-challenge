/*jshint laxcomma:true, smarttabs: true, mootools: true */
/**
 * Primary urls for application harness
 * @module urls
 * @author Eric Satterwhite
 **/
define({
	"ui/nav/reset":{controller:'weatherapp.controller', action:"resetnav" }
	,"challenge/{module}/{next*}":{controller:"weatherapp.controller", action:"index"}
});
