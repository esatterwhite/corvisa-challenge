## Corvisa Weather Coding Challenge
A working example of Corvisa Javascript weather application coding challenge. This solutions makes use of local storage, iconograpy, templating, routing / deep linking, built with AMD modules ontop of [requirejs](http://requirejs.org) and [Mootools](http://mootools.net)

*Switch branches to view different implementations*

### Build
To an optimized version of the application you must fist install [Node.js](http://nodejs.org) and a working installation of the [ruby](https://www.ruby-lang.org/en/downloads/) programming language.

#### Install

##### requirements

* ruby 1.9+
* Node.js
* sass 3.2.9
* compas 0.12.2

once node js is installed ,install the build dependancies, and run the build script
```

	gem install compass
	npm install 
	npm run-script build

```

### Test
To run the tests open the test.html file in a web browser.
